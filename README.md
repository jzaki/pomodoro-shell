# Pomodoro Shell

Commands to run a [pomodoro](https://en.wikipedia.org/wiki/Pomodoro_Technique) timer from the command line in ubuntu.

## Getting Started

Put the pomodoro technique at your fingertips.

### Prerequisites

Ubuntu.

[sound-icons](https://ubuntu.pkgs.org/18.04/ubuntu-main-i386/sound-icons_0.1-6_all.deb.html) (pre-installed) were used, but can refer to any sound file.

### Installing

Copy/paste this in a file where Terminal will find it (eg end of `~/.bashrc` ):

```
export SND_EP3=/usr/share/sounds/sound-icons/electric-piano-3.wav
export SND_XYL=/usr/share/sounds/sound-icons/xylofon.wav
alias beep="paplay $SND_EP3"
alias beepSoft="paplay $SND_XYL"

#Countdown function courtesy of - https://superuser.com/questions/611538/is-there-a-way-to-display-a-countdown-or-stopwatch-timer-in-a-terminal/611582#611582.
function countdown(){
   date1=$((`date +%s` + $1)); 
   while [ "$date1" -ge `date +%s` ]; do 
     echo -ne "$(date -u --date @$(($date1 - `date +%s`)) +%H:%M:%S)\r";
     sleep 1
   done
   echo "$1s elapsed";
}

function pomodoro(){
    endDate=$((`date +%s` + 25*60));
    echo "Finish $1: $(date --date @$(($endDate)) +%H:%M:%S)";
    countdown 25*60;
    echo "Time for a break."
    beepSoft;
    countdown 5*60;
    echo "Start your next tomato."
    beep;
}

```

### Running

In a new terminal (`Ctrl+Alt+t`) run: `pomodoro <what-you-will-finish>`

For a single timer: `countdown 60; beep`
